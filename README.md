# NSS-git-ukol

Tento repozitář slouží k vypracování úkolu na git z předmětu NSS

## Autoři

- Tomáš Klouček

## Použité technologie

- Git
- Vim
- zsh

## Popis

Jediný důvod tohoto repozitáře je vypracování úkolu z pššředmětu NSS

## Název instalace

- V tomto repu nic nemusíte instalovat protože je úplně k ničemu kromě úkolu na NSS

## Copyright

[MIT](https://choosealicense.com/licenses/mit/)

Copyright © 2023 Tomáš Klouček
